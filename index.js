'use strict';
const FileScanner = require("./lib/FileScanner");
const Sanitizer = require("./lib/Sanitizer");
module.exports = {
    FileScanner,
    Sanitizer
};
