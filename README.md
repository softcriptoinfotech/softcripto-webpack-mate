# Softcripto Webpack Mate

- FileScanner

A bundle of helpers and libraries for webpack.

## FileScanner

Scan a directory and return list of files based on pattern options provided.

### Usage
```javascript
'use strict';
const {FileScanner} = require("softcripto-webpack-mate");
const path = require('path');

const file = new FileScanner();
//Find all .html file from all folders in tmp directory
file.scan(path.resolve(__dirname, "tmp/**/*.html"));
console.log(file.files);
```

Output
```javascript
[ 
    { 
        filename: 'index.html',
        fullPath: '/to/home/root/tmp/viewer/finder/index.html',
        dir: 'finder' 
    },
    ...
]
```