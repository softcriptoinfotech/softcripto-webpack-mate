/**
 * Scan files pattern from given source directory
 * Return with bunch of details
 */

const glob = require('glob-all');
const path = require('path');

class FileScanner {
    /**
     * Read all files from specified source
     * and build list with Object
     * 
     * ### How to use
     * @param {String} source html location 
     * @example
     * const fileScanner = new FileScanner();
     * fileScanner.scan("theme/html/components/*.html");
     * fileScanner.files; //get files
     */
    constructor() {
        /**
         * @type {[{title: String, filename: String, file: String}]}
         */
        this._files = [];
    }
    /**
     * @returns {[{title: String, filename: String, file: String}]}
     */
    get files() { return this._files; }
    /**
     * Read directory to find *.html and add them to list
     * @param {String} source - relative path from root
     * @example
     * FileScanner.read('theme/html/components/*.html');
     */
    scan(source) {
        this._files = []; //reset
        glob.sync(source).forEach((file) => {
            this.add(file);
        });
    }
    /**
     * Push object into list
     * @param {string} file - a html file path
     */
    add(file) {
        let filename = path.basename(file);
        this._files.push({
            filename,
            fullPath: file,
            dir: this.getParentDirectory(file) //Parent folder that holds file
        });
    }
    /**
     * Get parent Directory
     * @param {string} fullPath 
     */
    getParentDirectory(fullPath) {
        let list = fullPath.split("/");
        return list[list.length - 2];
    }
}
//export
module.exports = FileScanner;