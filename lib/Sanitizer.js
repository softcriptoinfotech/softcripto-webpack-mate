'use strict';

class Sanitizer
{
    /**
     * Convert dash (-) separated string to human readable string
     * @param {string} filename filename in string
     * @param {string} ext - Extension to be removed
     * @returns {string}
     */
    static toHumanReadable(filename, ext) {
        let title = filename.replace(/\-/g, " "); //replace - to single space
        if (ext) {
            title = title.replace(ext, ''); //replace ext
        }
        return title.charAt(0).toUpperCase() + title.slice(1);
    }
    /**
     * Capitalise First Charactor of text.
     * @param {string} str string to be capitalised
     */
    static toFirstCharUpperCase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}

module.exports = Sanitizer;